var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var index = require('./routes/index');
var hbs = require('hbs');
var AsyncPolling = require('async-polling');
var Jimp = require("jimp");
var recursive = require('recursive-readdir');

hbs.registerPartial('partial_name', 'partial value');
hbs.registerPartials(__dirname + '/views/partials');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine','hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

AsyncPolling(function (end) {
  const folder = 'public/images/fulls/';
  const fs = require('fs');

var mkdirp = require('mkdirp');
recursive('./'+folder, function (err, files) {
    files.forEach(file => 
    {
      if (file.match(/\.(jpg|jpeg|JPG|JPEG)/g)) {
        var thumbFile = file.replace('fulls','thumbs');
        var folderPath = thumbFile.replace(path.basename(file),'');
        mkdirp(folderPath);
        
        while (file.indexOf('\\') > -1) {
          file=  file.replace('\\', '/');
        }
        while (thumbFile.indexOf('\\') > -1) {
          thumbFile=  thumbFile.replace('\\', '/');
        }

        var existFile = fs.existsSync(thumbFile);
        if (!existFile) { 
          Jimp.read('./'+file).then(function (lenna) {
                  lenna.scaleToFit(532,343.859)
                  .quality(90)
                  .write('./'+thumbFile);
              }).catch(
                function (err) {
                  console.error(err);
                });
        } 
      }
    });
  });

  end();
}, 3000).run();
