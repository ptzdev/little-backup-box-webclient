var express = require('express');
var router = express.Router();
var recursive = require('recursive-readdir');
const path = require('path');

router.get('/', function(req, res, next) {
  const folder = 'public/images/fulls/';
  const fs = require('fs');
  var photos =[];

  recursive('./'+folder, function (err, files) {
    files.forEach(file => {
     if (file.match(/\.(jpg|jpeg|JPG|JPEG)/g)) {
      var relativePath = folder;
      while (relativePath.indexOf('/') > -1) {
         relativePath=  relativePath.replace('/', '\\');
      }

      var pathFile = file.replace(relativePath,'');
      var fileNameThumbRelative= pathFile;
      while (fileNameThumbRelative.indexOf('\\') > -1) {
          fileNameThumbRelative=  fileNameThumbRelative.replace('\\', '/');
      }
      var photo = { 
          fileName:pathFile,
          thumb:fileNameThumbRelative,
          title:path.basename(file),
          description:'description',
      };

      photos.push(photo);
     }
    });
  });

  res.render('index', { 
      title: 'Little Backup Box',
      author : 'Pedro Torrezao',
      photos: photos 
    });
});

module.exports = router;


function getAbsolutePath(file) {
  return p.join('public/images/fulls/', file)
}

